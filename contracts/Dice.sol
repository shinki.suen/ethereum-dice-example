pragma solidity ^0.5.0;
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";

contract Dice is ERC721{

    enum diceState { stationary, rolling }

    struct dice {
        uint8 numberOfSides;
        uint8 color;
        uint8 currentNumber;  // number shown on dice (0 means its in 'rolling' state)
        diceState state;      // 'rolling' or 'rolled' state
        uint256 creationValue; // amount given to smart contract in ETH when dice is created
        uint256 luckytimes;   // number of times max number is rolled
    }

    event rolling (uint256 diceId);
    event rolled (uint256 diceId, uint8 newNumber);
    event luckytimesEvent(uint256 diceId);

    uint256 public numDices = 0;
    mapping(uint256 => dice) public dices;

    //function to create a new dice, and add to 'dices' map. requires at least 0.01ETH to create
    function add(
        uint8 numberOfSides,
        uint8 color
    ) public payable returns(uint256) {
        require(numberOfSides > 0, "Sides cannot be less than 1");
        require(msg.value > 0.01 ether, "Require a value of > 0.01 to create dice");

        //new dice object
        dice memory newDice = dice(
            numberOfSides,
            color,
            (uint8)(block.timestamp % numberOfSides) + 1,  //non-secure random number
            diceState.stationary,
            msg.value,
            0
        );

        uint256 newDiceId = numDices++;
        dices[newDiceId] = newDice;   //commit to state variable
        _mint(msg.sender, newDiceId); //create new dice using _mint()
        return newDiceId;             //return new diceId
    }

    //modifier to ensure a function is callable only by its owner
    modifier ownerOnly(uint256 diceId) {
        require(ownerOf(diceId) == msg.sender, "Owner only function");
        _;
    }

    modifier validDiceId(uint256 diceId) {
        require(diceId < numDices, "Invalid diceId");
        _;
    }

    //owner can roll a dice
    function roll(uint256 diceId) public ownerOnly(diceId) validDiceId(diceId) {
            dices[diceId].state = diceState.rolling;    //set state to rolling
            dices[diceId].currentNumber = 0;    //number will become 0 while rolling
            emit rolling(diceId);   //emit rolling event
    }

    function stopRoll(uint256 diceId) public ownerOnly(diceId) validDiceId(diceId) {
            dices[diceId].state = diceState.stationary; //set state to stationary

            //this is not a secure randomization
            uint8 newNumber = (uint8)((block.timestamp + diceId)*100 % dices[diceId].numberOfSides) + 1;
            dices[diceId].currentNumber = newNumber;
            if(dices[diceId].currentNumber == dices[diceId].numberOfSides){
                dices[diceId].luckytimes++;
                emit luckytimesEvent(diceId);
            }
            emit rolled(diceId, newNumber); //emit rolled
    }

    //get number of sides of dice
    function getDiceSides(uint256 diceId) public view validDiceId(diceId) returns (uint8) {
        return dices[diceId].numberOfSides;
    }

    //get current dice number
    function getDiceNumber(uint256 diceId) public view validDiceId(diceId) returns (uint8) {
        return dices[diceId].currentNumber;
    }

    //get current dice color
    function getDiceColor(uint256 diceId) public view validDiceId(diceId) returns (uint8) {
        return dices[diceId].color;
    }

    //get ether put in during creation
    function getDiceValue(uint256 diceId) public view validDiceId(diceId) returns (uint256) {
        return dices[diceId].creationValue;
    }

    function getLuckyTimes(uint256 diceId) public view validDiceId(diceId) returns(uint256){
        return dices[diceId].luckytimes;
    }

    function destroyDice(uint256 diceId) payable public ownerOnly(diceId) validDiceId(diceId) {
        msg.sender.transfer(dices[diceId].creationValue);
        _burn(diceId);
    }

}
