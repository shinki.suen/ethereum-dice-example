pragma solidity ^0.5.0;
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";

contract DiceToken is ERC20Mintable {

  string public constant name = "DiceToken";
  string public constant symbol = "DCT";
  uint8 public constant decimals = 18;

}
