pragma solidity ^0.5.0;
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./Dice.sol";

contract DiceMarket {

    Dice diceContract;  //reference to Dice.sol
    IERC20 token;       //reference to DiceToken.sol or any ERC20
    uint256 public commissionFee;
    address _owner = msg.sender;
    mapping(uint256 => uint256) listPrice;
    mapping(uint256 => address) prevOwner;

    constructor(Dice diceAddress, IERC20 ercAddress ,uint256 fee) public {
        diceContract = diceAddress;
        token = ercAddress;
        commissionFee = fee;
    }

    //list a dice. approve() to DiceMarket must have been already called
    //puts up a dice for sale at listing price 'price'
    function list(uint256 diceId, uint256 price) public {
      require(diceContract.ownerOf(diceId) == msg.sender, "Only owner can list");
      diceContract.transferFrom(msg.sender, address(this), diceId);
      prevOwner[diceId] = msg.sender;
      listPrice[diceId] = price;
    }

    //return to original owner without sale
    function unlist(uint256 diceId) public {
       require(msg.sender == prevOwner[diceId], "Only owner can unlist");
       diceContract.transferFrom(address(this), msg.sender, diceId);
       listPrice[diceId] = 0;
    }

    // get the listed price of a dice
    function checkPrice(uint256 diceId) public view returns (uint256) {
       return listPrice[diceId];
    }

    // get the original owner
    function checkOwner(uint256 diceId) public view returns (address) {
       return prevOwner[diceId];
    }

    // Buy the dice at the requested price contract
    // Must be greater than listed price + commissionFee
    // Commission is paid to owner of DiceMarket
    function buy(uint256 diceId, uint256 price) public {
       require(listPrice[diceId] != 0, "Unlisted dice"); //is listed
       require(price >= (listPrice[diceId] + commissionFee), "Price is too low");
       listPrice[diceId] = 0;
       require(token.transferFrom(msg.sender, _owner, commissionFee), "D1");
       require(token.transferFrom(msg.sender, prevOwner[diceId], (price - commissionFee)), "D2");
       diceContract.transferFrom(address(this), msg.sender, diceId);
    }

    //get owner of DiceMarket
    function getContractOwner() public view returns(address) {
       return _owner;
    }

}
