pragma solidity ^0.5.0;
import "./Dice.sol";

contract DiceBattle {

    Dice diceContract;  //reference to Dice.sol contract
    mapping(uint256 => address) _owner;

    constructor(Dice diceAddress) public {
        diceContract = diceAddress;
    }

    event battleWin(uint256 myDice,uint256 enemyDice);
    event battleDraw(uint256 myDice,uint256 enemyDice);

    //list a dice. approve() to DiceBattle must have been already called
    function list(uint256 diceId) public {
      require(diceContract.ownerOf(diceId) == msg.sender, "Only owner can list");
      diceContract.transferFrom(msg.sender, address(this), diceId);
      _owner[diceId] = msg.sender;
    }

    //return to original owner
    function unlist(uint256 diceId) public {
        require(_owner[diceId] != address(0), "Invalid diceId");
        require(_owner[diceId] == msg.sender, "Only owner can unlist");
        diceContract.transferFrom(address(this), _owner[diceId], diceId);
        _owner[diceId] = address(0);
    }

    //pick your dice to battle against another listed dice
    function battle(uint256 myDice, uint256 enemyDice) public {
        require(_owner[myDice] == msg.sender, "Only owner can initial battle");
        require(_owner[enemyDice] != address(0), "Invalid enemy diceId");

        diceContract.roll(myDice);
        diceContract.stopRoll(myDice);
        diceContract.roll(enemyDice);
        diceContract.stopRoll(enemyDice);

      address newOwner;
      if ( diceContract.getDiceNumber(myDice) == diceContract.getDiceNumber(enemyDice) ) {
          emit battleDraw(myDice,enemyDice);
          return;
      }

      if ( diceContract.getDiceNumber(myDice) > diceContract.getDiceNumber(enemyDice) ) {
          newOwner = _owner[myDice];
      } else {
          newOwner = _owner[enemyDice];
      }

      _owner[myDice] = newOwner;
      _owner[enemyDice] = newOwner;
      diceContract.transferFrom(address(this), newOwner, myDice);
      diceContract.transferFrom(address(this), newOwner, enemyDice);
      emit battleWin(myDice,enemyDice);
    }

}
