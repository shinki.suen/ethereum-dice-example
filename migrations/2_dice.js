const Dice = artifacts.require("Dice");
const DiceBattle = artifacts.require("DiceBattle");
const DiceToken = artifacts.require("DiceToken");
const DiceMarket = artifacts.require("DiceMarket");

module.exports = function(deployer, networks, accounts) {
  let platform = accounts[0];
  let diceInstance;
  let diceToken;
  let fee = 100;
  deployer.deploy(Dice, {from: platform})
  .then((_inst) => {
    diceInstance = _inst;
    return deployer.deploy(DiceBattle, diceInstance.address, {from: platform});
  }).then(() => {
    return deployer.deploy(DiceToken, {from: platform});
  }).then((_inst) => {
    diceToken = _inst;
    return deployer.deploy(DiceMarket, diceInstance.address, diceToken.address, fee, {from: platform});
  })
};
